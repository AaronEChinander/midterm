#!/bin/bash

OldFingerprints="$(find . -type f -exec md5sum {} \;)"


while true; do
sleep 5
CurrentFingerprints="$(find . -type f -exec md5sum {} \;)"
OldFingerprints="$(echo "$OldFingerprints" | sort)"
CurrentFingerprints="$(echo "$CurrentFingerprints" | sort)"
CreatedFiles=$(comm -3 <(echo "$CurrentFingerprints") <(echo "$OldFingerprints"))
DeletedFiles=$(comm -3 <(echo "$CurrentFingerprints") <(echo "$OldFingerprints"))
ModifiedFiles=$(comm -3 <(echo "$CurrentFingerprints") <(echo "$OldFingerprints"))
if [ -n "$CreatedFiles" ]; then
	echo "[+] File $CreatedFiles was created at $(date '+%Y-%m-%d_%H:%M:%S')"
fi

if [ -n "$ModifiedFiles" ]; then
	echo "[*] File $ModifiedFiles was changed at $(date '+%Y-%m-%d_%H:%M:%S:')"
fi

if [ -n "$DeletedFiles" ]; then
	echo "[-] File $DeletedFiles was deleted at $(date '+%Y-%m-%d_%H:%M:%S')"
fi

OldFingerprints=$(echo "$CurrentFingerprints")
done
